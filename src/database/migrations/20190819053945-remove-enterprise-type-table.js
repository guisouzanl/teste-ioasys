module.exports = {
  up: queryInterface => {
    return queryInterface.dropTable('enterprise_type');
  },

  down: queryInterface => {
    return queryInterface.dropTable('enterprise_type');
  },
};
