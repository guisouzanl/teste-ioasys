module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users', 'country', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'country');
  },
};
