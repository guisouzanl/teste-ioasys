module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('enterprises', 'enterprise_name', {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('enterprises');
  },
};
