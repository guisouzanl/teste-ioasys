module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users', 'enterprise', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'enterprise');
  },
};
