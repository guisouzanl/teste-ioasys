module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('enterprise_types', 'created_at', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('enterprise_types', 'created_at');
  },
};
