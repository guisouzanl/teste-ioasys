module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('enterprises', 'created_at', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('enterprises', 'created_at');
  },
};
