module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('enterprises', 'enterprise_type_id');
  },

  down: queryInterface => {
    return queryInterface.removeColumn('enterprises', 'enterprise_type_id');
  },
};
