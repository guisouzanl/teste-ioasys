module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('enterprises', 'photo_id', {
      type: Sequelize.INTEGER,
      references: { model: 'enterprise_files', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('enterprises', 'photo_id');
  },
};
