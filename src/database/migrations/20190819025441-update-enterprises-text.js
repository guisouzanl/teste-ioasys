module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('enterprises', 'description', {
      type: Sequelize.TEXT,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('enterprises');
  },
};
