module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('enterprises', 'enterprise_type_id', {
      type: Sequelize.INTEGER,
      references: { model: 'enterprise_type', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('enterprises', 'enterprise_type_id');
  },
};
