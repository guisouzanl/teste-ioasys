module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users', 'enterprise_type_id', {
      type: Sequelize.INTEGER,
      references: { model: 'enterprise_type', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'enterprise_type_id');
  },
};
