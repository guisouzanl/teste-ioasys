module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('enterprise_type', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      enterprise_type_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('enterprise_type');
  },
};
