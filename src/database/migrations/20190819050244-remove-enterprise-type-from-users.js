module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('users', 'enterprise_type_id');
  },

  down: queryInterface => {
    return queryInterface.dropTable('users');
  },
};
