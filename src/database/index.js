import Sequelize from 'sequelize';
import User from '../app/models/User';
import Enterprise from '../app/models/Enterprise';
import EnterpriseType from '../app/models/EnterpriseType';
import EnterpriseFile from '../app/models/EnterpriseFile';
import UserFile from '../app/models/UserFile';
import databaseConfig from '../config/database';

const models = [User, Enterprise, EnterpriseType, EnterpriseFile, UserFile];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    models.map(model => model.init(this.connection));
    models.map(
      model => model.associate && model.associate(this.connection.models)
    );
    // .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
