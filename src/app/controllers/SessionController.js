import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import User from '../models/User';
import authConfig from '../../config/auth';
import UserFile from '../models/UserFile';

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const { email, password } = req.body;

    const user = await User.findOne({
      include: [
        {
          model: UserFile,
          as: 'photo',
          attributes: ['path', 'url'],
        },
      ],
    });

    if (!user) {
      return res.status(401).json({
        success: false,
        error: 'Invalid login credentials. Please try again.',
      });
    }
    if (!(await user.checkPassword(password))) {
      return res.status(401).json({
        success: false,
        error: 'Invalid login credentials. Please try again.',
      });
    }

    const {
      id,
      investor_name,
      city,
      country,
      balance,
      enterprises_number,
      portfolio_value,
      super_angel,
      enterprise,
      first_access,
      photo,
    } = user;

    if (first_access === true) {
      await user.update({
        first_access: false,
      });
    }

    res.set({
      client: investor_name,
      uid: email,
      access_token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });

    return res.json({
      investor: {
        id,
        investor_name,
        city,
        country,
        balance,
        enterprises_number,
        portfolio_value,
        super_angel,
        first_access,
        photo,
        portfolio: {
          enterprises_number,
          enterprises: [],
        },
      },
      enterprise,
      success: true,
    });
  }
}

export default new SessionController();
