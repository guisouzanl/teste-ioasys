import UserFile from '../models/UserFile';

class UserFileController {
  async store(req, res) {
    const { originalname: name, filename: path } = req.file;

    const file = await UserFile.create({
      name,
      path,
    });
    return res.json(file);
  }
}

export default new UserFileController();
