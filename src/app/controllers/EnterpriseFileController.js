import EnterpriseFile from '../models/EnterpriseFile';

class EnterpriseFileController {
  async store(req, res) {
    const { originalname: name, filename: path } = req.file;

    const file = await EnterpriseFile.create({
      name,
      path,
    });

    return res.json(file);
  }
}

export default new EnterpriseFileController();
