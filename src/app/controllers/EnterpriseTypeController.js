import EnterpriseType from '../models/EnterpriseType';

class EnterpriseTypeController {
  async store(req, res) {
    const { enterprise_type_name } = req.body;

    const type = await EnterpriseType.create({
      enterprise_type_name,
    });

    return res.json(type);
  }
}

export default new EnterpriseTypeController();
