import * as Yup from 'yup';
import Sequelize from 'sequelize';
import Enterprise from '../models/Enterprise';
import EnterpriseType from '../models/EnterpriseType';
import EnterpriseFile from '../models/EnterpriseFile';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      enterprise_name: Yup.string().required(),
      description: Yup.string().required(),
      email_enterprise: Yup.string().email(),
      facebook: Yup.string(),
      twitter: Yup.string(),
      linkedin: Yup.string(),
      phone: Yup.string(),
      own_enterprise: Yup.boolean(),
      value: Yup.number(),
      shares: Yup.number(),
      share_price: Yup.number(),
      own_shares: Yup.number(),
      city: Yup.string(),
      country: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails!' });
    }

    const enterpriseExists = await Enterprise.findOne({
      where: { enterprise_name: req.body.enterprise_name },
    });

    if (enterpriseExists) {
      return res.status(400).json({ error: 'Enterprise already exists!' });
    }
    const {
      id,
      enterprise_name,
      description,
      email_enterprise,
      facebook,
      twitter,
      linkedin,
      phone,
      own_enterprise,
      value,
      shares,
      share_price,
      own_shares,
      city,
      country,
      enterprise_type_id,
    } = await Enterprise.create(req.body);

    return res.json({
      enterprise: {
        id,
        enterprise_name,
        description,
        email_enterprise,
        facebook,
        twitter,
        linkedin,
        phone,
        own_enterprise,
        value,
        shares,
        share_price,
        own_shares,
        city,
        country,
        enterprise_type_id,
      },
    });
  }

  async show(req, res) {
    const enterprise = await Enterprise.findByPk(req.params.id, {
      attributes: [
        'id',
        'enterprise_name',
        'description',
        'email_enterprise',
        'facebook',
        'twitter',
        'linkedin',
        'phone',
        'own_enterprise',
        'value',
        'shares',
        'share_price',
        'own_shares',
        'city',
        'country',
      ],
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_type',
          attributes: ['id', 'enterprise_type_name'],
        },
        {
          model: EnterpriseFile,
          as: 'photo',
          attributes: ['path', 'url'],
        },
      ],
    });

    if (enterprise) {
      return res.json({
        enterprise,
        success: true,
      });
    }

    return res.status(400).json({ error: 'Enterprise not found!' });
  }

  async index(req, res) {
    const { name, enterprise_types } = req.query;
    const lowName = name.toLowerCase();

    const enterprises = await Enterprise.findAll({
      where: {
        enterprise_name: Sequelize.where(
          Sequelize.fn('LOWER', Sequelize.col('enterprise_name')),
          'LIKE',
          `%${lowName}%`
        ),
        enterprise_type_id: enterprise_types,
      },
      attributes: [
        'id',
        'email_enterprise',
        'facebook',
        'twitter',
        'linkedin',
        'phone',
        'own_enterprise',
        'enterprise_name',
        'description',
        'city',
        'country',
        'value',
        'share_price',
      ],
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_type',
          attributes: ['id', 'enterprise_type_name'],
        },
        {
          model: EnterpriseFile,
          as: 'photo',
          attributes: ['path', 'url'],
        },
      ],
    });

    return res.json({
      enterprises,
    });
  }
}

export default new UserController();
