import * as Yup from 'yup';
import User from '../models/User';

class UserController {
  async store(req, res) {
    const schema = Yup.object().shape({
      investor_name: Yup.string().required(),
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string()
        .required()
        .min(6),
      city: Yup.string(),
      balance: Yup.number(),
      enterprises_number: Yup.number(),
      portfolio_value: Yup.number(),
      super_angel: Yup.boolean(),
      enterprise: Yup.string(),
      enterprises: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails!' });
    }

    const userExist = await User.findOne({ where: { email: req.body.email } });

    if (userExist) {
      return res.status(400).json({ error: 'User already exists!' });
    }
    const {
      id,
      investor_name,
      city,
      country,
      email,
      balance,
      enterprises_number,
      portfolio_value,
      super_angel,
      enterprise,
      first_access,
    } = await User.create(req.body);

    return res.json({
      investor: {
        id,
        investor_name,
        email,
        city,
        country,
        balance,
        portfolio: {
          enterprises_number,
          enterprises: [],
        },
        portfolio_value,
        first_access,
        super_angel,
      },
      enterprise,
    });
  }
}

export default new UserController();
