import jwt from 'jsonwebtoken';
import { promisify } from 'util';
import authConfig from '../../config/auth';
import User from '../models/User';

export default async (req, res, next) => {
  const { access_token, uid, client } = req.headers;

  if (!access_token) {
    return res.status(401).json({ error: 'Authorized users only.' });
  }

  try {
    const decoded = await promisify(jwt.verify)(
      access_token,
      authConfig.secret
    );

    req.userId = decoded.id;

    const { email, investor_name } = await User.findByPk(req.userId);

    if (email === uid && client === investor_name) {
      return next();
    }
    throw new Error();
  } catch (err) {
    return res.status(401).json({ error: 'Authorized users only.' });
  }
};
