import Sequelize, { Model } from 'sequelize';

class Enterprise extends Model {
  static init(sequelize) {
    super.init(
      {
        enterprise_name: Sequelize.STRING,
        description: Sequelize.TEXT,
        email_enterprise: Sequelize.STRING,
        facebook: Sequelize.STRING,
        twitter: Sequelize.STRING,
        linkedin: Sequelize.STRING,
        phone: Sequelize.STRING,
        own_enterprise: Sequelize.BOOLEAN,
        value: Sequelize.FLOAT,
        shares: Sequelize.INTEGER,
        share_price: Sequelize.FLOAT,
        own_shares: Sequelize.INTEGER,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.EnterpriseType, {
      foreignKey: 'enterprise_type_id',
      as: 'enterprise_type',
    });
    this.belongsTo(models.EnterpriseFile, {
      foreignKey: 'photo_id',
      as: 'photo',
    });
  }
}

export default Enterprise;
