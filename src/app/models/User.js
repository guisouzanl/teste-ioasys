import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        investor_name: Sequelize.STRING,
        email: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        balance: Sequelize.FLOAT,
        enterprises_number: Sequelize.INTEGER,
        portfolio_value: Sequelize.FLOAT,
        first_access: Sequelize.BOOLEAN,
        super_angel: Sequelize.BOOLEAN,
        enterprises: Sequelize.STRING,
        enterprise: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    this.addHook('beforeSave', async user => {
      if (user.password) {
        // eslint-disable-next-line no-param-reassign
        user.password_hash = await bcrypt.hash(user.password, 8);
      }
    });
    return this;
  }

  static associate(models) {
    this.belongsTo(models.UserFile, {
      foreignKey: 'photo_id',
      as: 'photo',
    });
  }

  checkPassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }
}

export default User;
