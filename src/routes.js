import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';
import UserController from './app/controllers/UserController';
import EnterpriseController from './app/controllers/EnterpriseController';
import EnterpriseTypeController from './app/controllers/EnterpriseTypeController';
import SessionController from './app/controllers/SessionController';
import EnterpriseFileController from './app/controllers/EnterpriseFileController';
import UserFileController from './app/controllers/UserFileController';
import authMiddleware from './app/middlewares/auth';

const routes = new Router();
const upload = multer(multerConfig);

routes.post('/api/users', UserController.store);
routes.post('/api/enterprises', EnterpriseController.store);
routes.post('/api/enterprisestype', EnterpriseTypeController.store);
routes.post('/api/users/auth/sign_in', SessionController.store);
routes.use(authMiddleware);
routes.post(
  '/api/filesenterprise',
  upload.single('file'),
  EnterpriseFileController.store
);
routes.post('/api/filesuser', upload.single('file'), UserFileController.store);
routes.get('/api/enterprises/:id', EnterpriseController.show);
routes.get('/api/enterprises', EnterpriseController.index);

export default routes;
