# Teste ioasys

O teste foi desenvolvido em NodeJS usando as seguintes dependências:

- bcryptjs = criptografar senha para salvar no banco
- dotenv = variáveis locais
- express = servidor
- jsonwebtoken = token
- multer = para imagens em multipartformdata
- mysql2 = dependência sequelize para sql
- sequelize = para criação de tabelas e utilização do banco
- yup = validação de campos

foi utilizado um banco mysql em docker na versão 5.7

## Rotas adicionais

Algumas rotas novas foram criadas para facilitar o uso da aplicação e para não inserir os itens diretamente no banco.

- Post /api/users = cria um novo usuário usando os mesmos atributos da api disponibilizada com validação
- Post /api/enterprises = cria uma nova enterprise usando os mesmos atributos da api disponibilizada com validação
- Post /api/enterprisestype = cria um novo tipo de enterprise para relacionamento com enterprise
- Post /api/filesenterprise = upload de photo para enterprise em multipartform com relacionamento
- Post /api/filesuser = upload de photo para user em multipartform com relacionamento
